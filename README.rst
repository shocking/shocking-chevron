****************
shocking-chevron
****************

`shocking`_ Plugin for the `chevron`_ Mustache Implementation

.. _`chevron`: https://github.com/noahmorrison/chevron
.. _`shocking`: https://gitlab.com/shocking/shocking

Configuration
=============

=================== ============== ==========================================================
Name                Default Value  Description
=================== ============== ==========================================================
extension           \".mustache\"  Extension used for template files
partials_directory  \"_partials\"  Directory containing partial files relative to the source
templates_directory \"_templates\" Directory containing template files relative to the source
=================== ============== ==========================================================

Notes
=====

* Both partial files and template files share the same file extension option.
  There is no harm in doing so since the partial file's contents are basically
  copy-and-pasted into the template prior to rendering.

* The ``partials_directory`` is not required, but errors may occur if a
  nonexistent partial is requested.

Licensing
=========

The shocking package is licensed under the terms of the `MIT/Expat license`_.

.. _`MIT/expat license`: https://spdx.org/licenses/MIT.html
