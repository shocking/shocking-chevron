__version__ = "0.1.0"
__version_info__ = (0, 1, 0)

import logging

import chevron
from shocking.plugin import BasePlugin

LOGGER = logging.getLogger("shocking")


class ShockingPlugin(BasePlugin):
    """ Mustache Layout Renderer Using chevron """

    _defaults = {
        "extension": ".mustache",
        "partial_directory": "_partials",
        "template_directory": "_templates",
    }

    def prepare(self):
        self.template_directory = self.config["source"].joinpath(self.config["template_directory"])

        if not self.template_directory.is_dir():
            LOGGER.error(f"Template directory '{self.template_directory}' does not exist")
            return False

        self.partial_directory = self.config["source"].joinpath(self.config["partial_directory"])

        if not self.partial_directory.is_dir():
            LOGGER.warning(f"Partials directory '{self.partial_directory}' does not exist")
            self.partial_directory = None

    def process(self, file_object, **metadata):
        if "layout" not in file_object.metadata:
            LOGGER.debug(f"No layout defined for '{file_object.path}'. Skipping")
            return file_object

        template_file_name = file_object.metadata["layout"] + self.config["extension"]
        template_file_path = self.template_directory.joinpath(template_file_name)

        if not template_file_path.is_file():
            LOGGER.error(f"Requested template file '{template_file_path}' does not exist")
            return

        render = chevron.render(
            template_file_path.read_text(),
            file_object.metadata,
            partials_ext=self.config["extension"][1:],
            partials_path=str(self.partial_directory),
            partials_dict={"content": file_object.content}
        )

        return file_object._replace(content=render)
